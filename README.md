# Site web ATM64 V2.0

Magasin de vente/réparation d'appareils de communication. Salies de Béarn 64270 - France

_Statut: En cours de dév...

Customisation d'un [Bootstrap](http://getbootstrap.com/) + thème [Barberz par COLORLIB](https://colorlib.com).

----

# Licenses

Le framework original (Bootstrap) est placé sous licence [MIT]()    
Le template Barberz par Colorlib est placé sous license [CC BY 3.0]()

Les icônes et logos sont des créations/dérivations personnelles. Certaines sources sont issues de la librairie [OpenClipArt](http://openclipart.org/), et élevées par anticipation dans le domaine public ([CC-0-PD](http://creativecommons.org/publicdomain/zero/1.0/deed.fr)). Tous les travaux originaux ou dérivés ont été reversées à la librairie OpenCLipArt.

Les photographies sont issues de la librairie [Wikimedia Commons](https://commons.wikimedia.org), et sous diverses licences libres (cf. détail ci-après)

  -

# To-do

- [ ] Relecture et correction des fôtedortograff/grammaire
- [x] Ajouter un bouton de retour en haut de page
- [ ] Boutons liens médias sociaux
- [ ] Tester le formulaire de contact (mail) via php
- [ ] Illustrations manquantes (page Boutique, Manifeste)
- [ ] "Nice URLs" (~~.html~~)
- [ ] ...
